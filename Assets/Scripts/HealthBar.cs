using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Image barImage;
    [SerializeField]
    private Image barDamage;
    [SerializeField]
    private Character source;
    [SerializeField]
    private float maxValue;

    // Start is called before the first frame update
    void Start()
    {
        if (source)
        {
            maxValue = source.Health;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ChangeValue();
    }

    private void ChangeValue()
    {
        if (source)
        {
            barImage.fillAmount = source.Health / maxValue;

            if (barDamage)
            {
                barDamage.fillAmount = Mathf.Lerp(barDamage.fillAmount, barImage.fillAmount, Time.deltaTime * 2);
            }
        }
    }
}
