using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hurt : MonoBehaviour
{
    [SerializeField]
    private int damage;
    [SerializeField]
    private Character source;

    // Start is called before the first frame update
    void Start()
    {
        //
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Character target = collision.GetComponent<Character>();

        if (target && target.IsAlive() && !target.Equals(source))
        {
            if (!target.Hited)
            {
                target.Health -= damage;
                target.GotHited(0.5f);
            }
        }
    }
}
