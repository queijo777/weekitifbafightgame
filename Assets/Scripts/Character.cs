using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    [SerializeField]
    private int health = 10;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private Collider2D boxCollider;
    [SerializeField]
    private LayerMask whatIsGround;
    [SerializeField]
    private string currentAnimation = "Idle";
    [SerializeField]
    private string newAnimation = "";
    [SerializeField]
    private bool jumping = false;
    [SerializeField]
    private bool hited = false;
    [SerializeField]
    private bool isDead = false;

    private Rigidbody2D body;
    private Animator anim;

    public int Health { get => health; set => health = value; }
    public float Speed { get => speed; set => speed = value; }
    public Rigidbody2D Body { get => body; set => body = value; }
    public Animator Anim { get => anim; set => anim = value; }
    public float JumpForce { get => jumpForce; set => jumpForce = value; }
    public Collider2D BoxCollider { get => boxCollider; set => boxCollider = value; }
    public LayerMask WhatIsGround { get => whatIsGround; set => whatIsGround = value; }
    public string CurrentAnimation { get => currentAnimation; set => currentAnimation = value; }
    public string NewAnimation { get => newAnimation; set => newAnimation = value; }
    public bool IsDead { get => isDead; set => isDead = value; }
    public bool Hited { get => hited; set => hited = value; }
    public bool Jumping { get => jumping; set => jumping = value; }

    protected void Start()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<Collider2D>();
    }

    public bool IsAlive()
    {
        if (health <= 0)
        {
            Die();
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool IsAttacking()
    {
        return CurrentAnimation.Contains("Atk");
    }

    public void GotHited(float time)
    {
        hited = true;

        StartCoroutine(ShowHitEffect(time));
    }

    private void Die()
    {
        if (Health <= 0 && !IsDead)
        {
            IsDead = true;
            Body.velocity = Vector2.zero;
            StartCoroutine(Dying());
        }
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private IEnumerator ShowHitEffect(float time)
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.color = Color.red;

        yield return new WaitForSeconds(time);

        spriteRenderer.color = Color.white;

        hited = false;
    }

    private IEnumerator Dying()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        for (int i = 0; i < 3; i++)
        {
            spriteRenderer.color = Color.clear;
            yield return new WaitForSeconds(0.15f);
            spriteRenderer.color = Color.white;
            yield return new WaitForSeconds(0.15f);
        }

        spriteRenderer.color = Color.clear;

        gameObject.SetActive(false);

        Invoke("ReloadScene", 3f);
    }
}
