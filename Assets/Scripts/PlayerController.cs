using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character
{
    [SerializeField]
    private bool isControllable = true;
    [SerializeField]
    private List<ControlMaps> controlsMap = new List<ControlMaps>
    {
        new("Jump", KeyCode.Space),
        new("LightAtk", KeyCode.Q),
        new("Jump", KeyCode.W)
    };

    public bool IsControllable { get => isControllable; set => isControllable = value; }

    [SerializeField]
    private Character target;

    new private void Start()
    {
        base.Start();

        if (controlsMap.Count == 0)
        {
            SetDefaultControls();
        }
        else if (tag.Contains("Player"))
        {
            SetControlsByPlayer();
        }

        FindTarget();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isControllable)
        {
            return;
        }

        if (!IsAlive())
        {
            return;
        }

        Animate();

        if (Attack())
            return;

        Jump();

        Move();

        FlipToTarget();
    }

    private void Animate()
    {
        // Checa se a anima��o atual � igual a nova
        if (!NewAnimation.Equals(CurrentAnimation))
        {
            // Torna a anima��o atual igual a nova
            CurrentAnimation = NewAnimation;

            // Toca a anima��o
            Anim.Play(CurrentAnimation);
        }
    }

    private bool Attack()
    {
        if (IsAttacking())
            return true;

        if (Input.GetKeyDown(GetActionKey("LightAtk")))
        {
            NewAnimation = "LightAtk";
            Body.velocity = Vector2.zero;
            return true;
        } 
        else if (Input.GetKeyDown(GetActionKey("HeavyAtk")) && BoxCollider.IsTouchingLayers(WhatIsGround))
        {
            NewAnimation = "HeavyAtk";
            Body.velocity = Vector2.zero;
            return true;
        }

        return false;
    }

    private void Move()
    {
        if (IsAttacking())
            return;

        bool onGround = BoxCollider.IsTouchingLayers(WhatIsGround);

        float moveSpeed = onGround ? Speed : Speed / 2f;

        float direction = GetDirection();

        Body.velocity = new Vector2(direction * moveSpeed, Body.velocity.y);

        if (!Jumping)
        {
            NewAnimation = direction != 0 ? "Walk" : "Idle";
        }
    }

    private void Jump()
    {
        if (IsAttacking() || Jumping)
            return;

        if (Input.GetKeyDown(GetActionKey("Jump")) && BoxCollider.IsTouchingLayers(WhatIsGround))
        {
            Body.AddForce(new Vector2(0, JumpForce));
            NewAnimation = "Jump";
            StartCoroutine(DoJump());
        }
    }

    public void EndAtk()
    {
        NewAnimation = "Idle";
    }

    public void FlipToTarget()
    {
        if (IsAttacking())
        {
            return;
        }

        if (target)
        {
            if ((transform.position.x > target.transform.position.x && !IsLookingLeft()) || (transform.position.x < target.transform.position.x && IsLookingLeft()))
            {
                Flip();
            }
        }
    }

    private void Flip()
    {
        float xScale = transform.localScale.x;

        xScale *= -1;

        transform.localScale = new Vector3(xScale, transform.localScale.y, transform.localScale.z);
    }

    public void FindTarget()
    {
        string targetTag = CompareTag("Player1") ? "Player2" : "Player1";

        GameObject player = GameObject.FindGameObjectWithTag(targetTag);

        if (player)
        {
            target = player.GetComponent<Character>();
        }
    }

    public bool IsLookingLeft()
    {
        return transform.localScale.x < 0;
    }

    private void SetDefaultControls()
    {
        controlsMap = new List<ControlMaps>();

        controlsMap.Add(new("Jump", KeyCode.Space));
        controlsMap.Add(new("LightAtk", KeyCode.Q));
        controlsMap.Add(new("HeavyAtk", KeyCode.W));
    }

    private void SetControlsByPlayer()
    {
        controlsMap = new List<ControlMaps>();

        if (CompareTag("Player1"))
        {
            controlsMap.Add(new("Jump", KeyCode.Space));
            controlsMap.Add(new("LightAtk", KeyCode.Q));
            controlsMap.Add(new("HeavyAtk", KeyCode.W));
        } 
        else if (CompareTag("Player2"))
        {
            controlsMap.Add(new("Jump", KeyCode.Keypad0));
            controlsMap.Add(new("LightAtk", KeyCode.Keypad2));
            controlsMap.Add(new("HeavyAtk", KeyCode.Keypad3));
        }
    }

    private float GetDirection()
    {
        if (CompareTag("Player"))
        {
            return Input.GetAxisRaw("Horizontal");
        }

        if (CompareTag("Player1"))
        {
            if (Input.GetKey(KeyCode.A))
            {
                return -1;
            }

            if (Input.GetKey(KeyCode.D))
            {
                return 1;
            }

            return 0;
        }
        else if (CompareTag("Player2"))
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                return -1;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                return 1;
            }

            return 0;
        }

        return 0;
    }

    private IEnumerator DoJump()
    {
        Jumping = true;

        yield return new WaitForSeconds(0.3f);

        while (!BoxCollider.IsTouchingLayers(WhatIsGround))
        {
            yield return new WaitForEndOfFrame();
        }

        Jumping = false;
    }

    public KeyCode GetActionKey(string animation)
    {
        foreach (ControlMaps c in controlsMap)
        {
            if (c.animationAction.Equals(animation))
            {
                return c.keyCode;
            }
        }

        return KeyCode.None;
    }

    [System.Serializable]
    public class ControlMaps
    {
        public string animationAction;
        public KeyCode keyCode;

        public ControlMaps(string animationAction, KeyCode keyCode)
        {
            this.animationAction = animationAction;
            this.keyCode = keyCode;
        }
    }
}
